# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi

if [ -e /home/lumi/.nix-profile/etc/profile.d/nix.sh ]; then . /home/lumi/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
