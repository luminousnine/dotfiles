;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu))
(use-service-modules cups desktop linux networking ssh xorg)

(operating-system
  (locale "en_GB.utf8")
  (timezone "Europe/Helsinki")
  (keyboard-layout (keyboard-layout "fi"))
  (host-name "guixie")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "lumi")
                  (comment "Lumi")
                  (group "users")
                  (home-directory "/home/lumi")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (list (specification->package "fuse")
                          (specification->package "gvfs")
                          (specification->package "ntfs-3g")
                          (specification->package "curl")
                          (specification->package "git")
                          (specification->package "protonvpn-cli")
                          (specification->package "pinentry")
                          (specification->package "pinentry-tty")
                          (specification->package "pinentry-gtk2")
                          (specification->package "pulseaudio")
                          (specification->package "icecat")
                          (specification->package "librewolf")
                          (specification->package "fontconfig")
                          (specification->package "gnupg")
                          (specification->package "i3-wm")
                          (specification->package "i3blocks")
                          (specification->package "i3lock")
                          (specification->package "dmenu"))
                    %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list (service xfce-desktop-service-type)
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout)))
                  (service zram-device-service-type
                  (zram-device-configuration
                  (size "8G")
                  (compression-algorithm 'zstd)
                  (priority 10))))
           ;; This is the default list of services we
           ;; are appending to.
           %desktop-services))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "ecd92fa3-ac0a-4f47-a909-f75884d70cb8")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "D0F1-391B"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "0c1ac2f4-2f43-42c9-86a5-f5debf6241dc"
                                  'btrfs))
                         (type "btrfs")) %base-file-systems)))
