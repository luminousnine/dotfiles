;; Lumi's config
;; Steal all you want.

(use-modules 
  (gnu home)
  (gnu home services)
  (gnu home services desktop)
  (gnu home services gnupg)
  (gnu home services guix)
  (gnu home services mcron)
  (gnu home services pm)
  (gnu home services shells)
  (gnu packages)
  (gnu packages admin)
  (gnu packages aspell)
  (gnu packages base)
  (gnu packages bootloaders)
  (gnu packages build-tools)
  (gnu packages compression)
  (gnu packages compton)
  (gnu packages databases)
  (gnu packages ebook)
  (gnu packages emacs-xyz)
  (gnu packages fonts)
  (gnu packages freedesktop)
  (gnu packages ftp)
  (gnu packages games)
  (gnu packages gnome)
  (gnu packages gnome-xyz)
  (gnu packages gnupg)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages haskell)
  (gnu packages haskell-xyz)
  (gnu packages image)
  (gnu packages image-viewers)
  (gnu packages linux)
  (gnu packages lisp)
  (gnu packages llvm)
  (gnu packages lxde)
  (gnu packages mail)
  (gnu packages man)
  (gnu packages moreutils)
  (gnu packages ncurses)
  (gnu packages package-management)
  (gnu packages password-utils)
  (gnu packages pdf)
  (gnu packages pulseaudio)
  (gnu packages pv)
  (gnu packages racket)
  (gnu packages rsync)
  (gnu packages ruby)
  (gnu packages sdl)
  (gnu packages shells)
  (gnu packages shellutils)
  (gnu packages skribilo)
  (gnu packages ssh)
  (gnu packages terminals)
  (gnu packages texlive)
  (gnu packages text-editors)
  (gnu packages texinfo)
  (gnu packages tls)
  (gnu packages version-control)
  (gnu packages video)
  (gnu packages vpn)
  (gnu packages web-browsers)
  (gnu packages wm)
  (gnu packages xfce)
  (gnu packages xorg)
  (gnu packages xdisorg)
  (gnu services)
  (gnu services mcron)
  (guix build-system copy)
  (guix build-system emacs)
  (guix build utils)
  (guix channels)
  (guix download)
  (guix gexp)
  (guix git-download)
  ((guix licenses) #:prefix license:)
  (guix packages)
  (guix transformations)
  (guix utils)
  (ice-9 format)
  (ice-9 match)
  (ice-9 popen)
  (ice-9 textual-ports))

(home-environment
 (packages (specifications->packages (list  "font-google-noto"
                                            "font-google-noto-emoji"
                                            "font-awesome"
                                            "font-gnu-unifont"
                                            "font-hack"
                                            "font-iosevka"
                                            "font-liberation"
                                            "font-openmoji"
                                            "font-terminus"
                                            "bsd-games"
                                            "calibre"
                                            "dunst"
                                            "evince"
                                            "feh"
                                            "filezilla"
                                            "flatpak"
                                            "gcc-toolchain"
                                            "glibc-locales"
                                            "gtk+:bin"
                                            "gucharmap"
                                            "guile-reader"
                                            "haunt"
                                            "irssi"
                                            "aspell"
                                            "ispell"
                                            "isync"
                                            "keepassxc"
                                            "krita"
                                            "lagrange"
                                            "libnotify"
                                            "libreoffice"
                                            "libressl"
                                            "lxappearance"
                                            "mpv"
                                            "mu"
                                            "neofetch"
                                            "neovim"
                                            "network-manager"
                                            "network-manager-applet"
                                            "network-manager-openvpn"
                                            "notmuch"
                                            "pandoc"
                                            "papirus-icon-theme"
                                            "pamixer"
                                            "pasystray"
                                            "picom"
                                            "pipe-viewer"
                                            "polybar"
                                            "obs"
                                            "offlineimap3"
                                            "openssl"
                                            "openssh"
                                            "openvpn"
                                            "password-store"
                                            "qutebrowser"
                                            "racket"
                                            "rofi"
                                            "rsync"
                                            "sbcl"
                                            "scrot"
                                            "setxkbmap"
                                            "skribilo"
                                            "surfraw"
                                            "texlive"
                                            "texinfo"
                                            "texstudio"
                                            "wireguard-tools"
                                            "xfce4-screenshooter"
                                            "xhost"
                                            "xprop"
                                            "xrdb"
                                            "xset"
                                            "unzip"
                                            "unicode-emoji"
                                            "yt-dlp"
                                            "zip"
               "emacs"
               "emacs-exwm"
               "emacs-all-the-icons"
               "emacs-all-the-icons-dired"
               "emacs-all-the-icons-ibuffer"
               "emacs-all-the-icons-completion"
               "emacs-auctex"
               "emacs-auto-complete"
               "emacs-bash-completion"
               "emacs-calibredb"
               "emacs-circe"
               "emacs-counsel"
               "emacs-counsel-projectile"
               "emacs-desktop-environment"
               "emacs-dired-git-info"
               "emacs-dired-sidebar"
               "emacs-dirvish"
               "emacs-dmenu"
               "emacs-doom-modeline"
               "emacs-doom-themes"
               "emacs-eat"
               "emacs-eglot"
               "emacs-elfeed"
               "emacs-elfeed-org"
               "emacs-elpher"
               "emacs-ement"
               "emacs-emms"
               "emacs-enlight"
               "emacs-fontaine"
               "emacs-geiser"
               "emacs-geiser-guile"
               "emacs-geiser-racket"
               "emacs-general"
               "emacs-gnugo"
               "emacs-guix"
               "emacs-helpful"
               "emacs-hydra"
               "emacs-ivy"
               "emacs-ivy-posframe"
               "emacs-ivy-rich"
               "emacs-jabber"
               "emacs-lsp-mode"
               "emacs-magit"
               "emacs-markdown-mode"
               "emacs-mastodon"
               "emacs-muse"
               "emacs-nerd-icons"
               "emacs-notmuch"
               "emacs-nov-el"
               "emacs-counsel-notmuch"
               "emacs-org"
               "emacs-org-beautify-theme"
               "emacs-org-books"
               "emacs-org-brain"
               "emacs-org-bullets"
               "emacs-org-journal"
               "emacs-org-pomodoro"
               "emacs-org-ref"
               "emacs-org-roam"
               "emacs-org-superstar"
               "emacs-org-tanglesync"
               "emacs-org-web-tools"
               "emacs-ox-haunt"
               "emacs-ox-pandoc"
               "emacs-paredit"
               "emacs-pass"
               "emacs-password-generator"
               "emacs-password-store"
               "emacs-pdf-tools"
               "emacs-saveplace-pdf-view"
               "emacs-projectile"
               "emacs-pulseaudio-control"
               "emacs-racket-mode"
               "emacs-rainbow-delimiters"
               "emacs-ripgrep"
               "emacs-slime"
               "emacs-sly"
               "emacs-sly-asdf"
               "emacs-sly-macrostep"
               "emacs-sly-quicklisp"
               "emacs-spaceline"
               "emacs-synosaurus"
               "emacs-swiper"
               "emacs-templatel"
               "emacs-tldr"
               "emacs-use-package"
               "emacs-web-mode"
               "emacs-webpaste"
               "emacs-websocket"
               "emacs-which-key"
               "emacs-writegood-mode"
               "emacs-writeroom"
               "emacs-znc"
               "emacs-0x0")))

  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (bashrc (list (local-file "/home/lumi/guix//.bashrc"
                                             "bashrc")))
                   (bash-profile (list (local-file
                                        "/home/lumi/guix//.bash_profile"
                                        "bash_profile")))
                   (environment-variables '(("EDITOR" . "nvim")
                                      ("VISUAL" . "nvim")
                                      ("GPG_TTY" . "(tty)")
				      ("LANG" . "en_GB.UTF-8")
				      ("XDG_CONFIG_HOME" . "$HOME/.config")
				      ("PATH" . "$PATH:$HOME/.local/bin")
                                      ("GUIX_LOCPATH" . "$HOME/.guix-home/profile/lib/locale")))))
;          (extra-special-file
;             "~/.config/i3/config" (local-file "i3-config"))
          (service home-dbus-service-type)
          (simple-service 'mymcron home-mcron-service-type
              ;; Every day at 18:00
                (list #~(job '(next-hour '(18))
                         "./$HOME/sync.sh")))
          (service home-gpg-agent-service-type
                  (home-gpg-agent-configuration
                  (pinentry-program
                   (file-append pinentry "/bin/pinentry"))
                   (ssh-support? #t)
                   (default-cache-ttl 3000)
                   (max-cache-ttl 6000)
                   (extra-content "\
          allow-loopback-pinentry
          allow-emacs-pinentry\n")))

  )))
